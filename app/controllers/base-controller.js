import Ember from 'ember';

export default Ember.Controller.extend({
    
    rightAnswer: null,

    currentView: null,

    answer: "",

    nextView: function() {
        return "question"+(this.currentView+1);
    },

    check: function(answer) {
        return answer === this.rightAnswer;
        //return true;
    },

    onSubmit: function(answer) {
        console.log("Answer", answer);
        if(this.check(answer)) {
            this.correctSound();
            this.transitionToRoute(this.nextView());
        } else {
            this.wrongSound();
        }
    },

    send: function(event) {
        this.onSubmit(this.get("answer"));
    },

    correctSound: function() {
        document.getElementById("ok").play();
    },

    wrongSound: function() {
        document.getElementById("wrong").play();
    }
});
