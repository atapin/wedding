import BaseController from './base-controller';

export default BaseController.extend({
    currentView: 2,
    rightAnswer: 'элефант',
    question: "Где было наше первое свидание?",

    check: function(answer) {
        return this.rightAnswer === answer.toLowerCase();
    },
});
