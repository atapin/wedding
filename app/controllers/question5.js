import BaseController from './base-controller';

export default BaseController.extend({
    currentView: 5,
    rightAnswer: '24.03.2015',
    question: "Когда мы подали заявление в ЗАГС?",

    nextView: function() {
        return "finish";
    },

    correctSound: function() {
        document.getElementById("win").play();
    }
});
