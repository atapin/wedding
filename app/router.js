import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('question1');
  this.route('question2');
  this.route('question3');
  this.route('question4');
  this.route('question5');
  this.route('question6');
  this.route('finish');
});

export default Router;
