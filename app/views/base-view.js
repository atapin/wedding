import Ember from 'ember';

export default Ember.View.extend({
    templateName: "question",

    didInsertElement: function(){
        //called on creation
        this.$().hide().fadeIn(500);
    },
    willDestroyElement: function(){
        //called on destruction
        this.$().slideDown(500);
    }
});
